import React, {useEffect, useContext} from 'react'
import { Link, useHistory } from 'react-router-dom'
import Axios from 'axios'
import { Table, Image, Button, message, Row, Col } from 'antd';
import { FileAddOutlined } from '@ant-design/icons';
import {Context} from '../store'
import FormFilterMovie from '../components/FormFilterMovie';

const ManageMovie = () => {
  const [user,,,,movies,,action] = useContext(Context)
  const history = useHistory()

  useEffect(() => {
    console.log('masuk useEffect');
    if(user.name){
      action.fetchMovies()
    } else {
      history.push('/login')
    }
  }, [])
  
  const handleDelete = (ID_MOVIE) => {
    Axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${ID_MOVIE}`, {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    })
    .then(() => {
      action.fetchMovies()
      message.success('Delete Data Success')
    }).catch((err) => {
      console.log(err.response);
      message.error('Delete Data Failed!')
    });
  }

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      sorter: (a, b) => Number(a.id) - Number(b.id),
    },
  
    {
      title: 'Poster Movie',
      dataIndex: 'image_url',
      render: (i) => {
        return <Image width={100} src={i}/>
      }
    },
  
    {
      title: 'Title',
      dataIndex: 'title',
      sorter: (a, b) => {
        if(a.title < b.title) { return -1; }
        if(a.title > b.title) { return 1; }
        return 0;}
    },
  
    {
      title: 'Genre',
      dataIndex: 'genre',
      sorter: (a, b) => {
        if(a.genre < b.genre) { return -1; }
        if(a.genre > b.genre) { return 1; }
        return 0;}
    },
  
    {
      title: 'Description',
      dataIndex: 'description',
      sorter: (a, b) => a.description.length - b.description.length,
    },

    {
      title: 'Review',
      dataIndex: 'review',
      sorter: (a, b) => a.review.length - b.review.length,
    },
  
    {
      title: 'Rating',
      dataIndex: 'rating',
      sorter: (a, b) => Number(a.rating) - Number(b.rating),
    },

    {
      title: 'Duration',
      dataIndex: 'duration',
      sorter: (a, b) => Number(a.duration) - Number(b.duration),
    },

    {
      title: 'Year',
      dataIndex: 'year',
      sorter: (a, b) => Number(a.year) - Number(b.year),
    },
  
    {
      title: 'Action',
      dataIndex: 'id',
      key: 'x',
      render: (id) => (
        <>
          <a><Link to={`/movies/${id}`}>Detail</Link></a><br />
          <a><Link to={`/movies/edit/${id}`}>Edit</Link></a><br />
          <a onClick={() => handleDelete(id)}>Delete</a>
        </>
      ),
    },
  ];

  return (
    <div>
      <h1>Manage Movies</h1>
      <Row justify='end'>
        <Link to='/movies/create' style={{float: "right", margin: '10px'}} >
          <Button type="primary" icon={<FileAddOutlined />}>
            Add New Movie
          </Button>
        </Link>
      </Row>
      <Row>
        <Col span={4}>
          <FormFilterMovie />
        </Col>
        <Col span={20}>
          <Table columns={columns} dataSource={movies} />
        </Col>
      </Row>
    </div>
  )
}

export default ManageMovie
