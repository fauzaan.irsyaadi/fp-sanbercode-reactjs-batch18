import React, {useContext, useEffect, useState} from 'react'
import { Form, Input, Button, Select, InputNumber, Checkbox, Row, Col, message } from 'antd';
import {Context} from '../store'
import Axios from 'axios'
import { useParams, useHistory } from 'react-router-dom';

const { Option } = Select

const layout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 10,
  },
};
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
    number: '${label} is not a validate number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

const CreateGame = () => {
  const [user, , , , , , action] = useContext(Context)
  const [edit, setEdit] = useState(false)
  const [form] = Form.useForm();

  let {id} = useParams()
  let history = useHistory()

  useEffect(() => {
    if(id){
      setEdit(true)
      fetchDataEdit(id)
    }
  }, [id])

  const fetchDataEdit = (ID_GAMES) => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-game/${ID_GAMES}`)
    .then(({data}) => {
      console.log(data);
      let dataEdit = {
        release: data.release,
        name: data.name,
        image_url: data.image_url,
        platform: data.platform,
        genre: data.genre.split(', '),
        modePlayer: [],
      }
      if(Number(data.singlePlayer) === 1){
        dataEdit.modePlayer.push('singlePlayer')
      }
      if(Number(data.multiplayer) === 1){
        dataEdit.modePlayer.push('multiplayer')
      }
      form.setFieldsValue(dataEdit)
    }).catch((err) => {
      console.log(err.response);
    });
  }

  const saveGames = (data) => {
    Axios.post(`https://backendexample.sanbersy.com/api/data-game`,
      data,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
          'Access-Control-Allow-Origin': true
        }
      }
    )
    .then(() => {
      message.success('Add Game Success')
      form.resetFields()
      action.fetchGames()
      history.push('/games/manage')
    }).catch((err) => {
      message.error(JSON.stringify(err.response.data))
      console.log(err.response);
      form.resetFields()
      if(err.response.data.status === 'Token is Expired'){
        console.log('Token udah expired');
        action.userLogout()
        history.push('/login')
      }
    });
  }

  const updateGames = (data) => {
    Axios.put(`https://backendexample.sanbersy.com/api/data-game/${id}`,
      data,
      {
        headers: {
          Authorization: `Bearer ${user.token}`
        }
      }
    )
    .then(() => {
      message.success('Edit Game Success')
      form.resetFields()
      setEdit(false)
      action.fetchGames()
      history.push('/games/manage')
    }).catch((err) => {
      message.error(JSON.stringify(err.response.data))
      form.resetFields()
      console.log(err.response);
      if(err.response.data.status === 'Token is Expired'){
        console.log('Token udah expired');
        action.userLogout()
        history.push('/login')
      }
    });
  }

  const onFinish = (val) => {
    let data = {
      name: val.name,
      image_url: val.image_url,
      platform: val.platform,
      release: val.release,
      singlePlayer: 0,
      multiplayer: 0,
      genre: val.genre.join(', ')
    }
    val.modePlayer.forEach((mode) => {
      data[mode] = 1
    })
    if(edit){
      updateGames(data)
    } else {
      saveGames(data)
    }
    
  };

  let initial = {
    release: 2020,
    name: '',
    image_url: '',
    platform: '',
    genre: [],
    modePlayer: ['singlePlayer'],
  }

  return (
    <>
      {
        edit
        ? <h1 style={{textAlign: 'center'}}>Edit Game</h1>
        : <h1 style={{textAlign: 'center'}}>Create New Game</h1>
      }
      <Form {...layout} form={form} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages} initialValues={initial}>
        <Form.Item
          name='name'
          label="Game Title"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="genre"
          label="Genre"
          rules={[
            {
              required: true,
              message: 'Select game genre',
              type: 'array',
            },
          ]}
        >
          <Select mode="multiple" placeholder="Select game genre">
            <Option value="Action">Action</Option>
            <Option value="War">War</Option>
            <Option value="Horror">Horror</Option>
            <Option value="Survival">Survival</Option>
            <Option value="Fantacy">Fantacy</Option>
            <Option value="Thriller">Thriller</Option>
            <Option value="Fiction">Fiction</Option>
            <Option value="Non-Fiction">Non-Fiction</Option>
            <Option value="Education">Education</Option>
            <Option value="History">History</Option>
            <Option value="Stealth">Stealth</Option>
            <Option value="Comedy">Comedy</Option>
            <Option value="Mistery">Mistery</Option>
            <Option value="Romance">Romance</Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="platform"
          label="Platform"
          hasFeedback
          rules={[
            {
              required: true,
              message: 'select game platform',
            },
          ]}
        >
          <Select placeholder="select game platform">
            <Option value="Windows">Windows</Option>
            <Option value="Playstation">Playstation</Option>
            <Option value="Xbox">Xbox</Option>
            <Option value="Nintendo">Nintendo</Option>
            <Option value="Android">Android</Option>
            <Option value="IOS">IOS</Option>
          </Select>
        </Form.Item>

        <Form.Item label="Release" rules={[
          {
            required: true,
            message: 'input game release!',
            type: 'number' 
          },
        ]}>
          <Form.Item name="release" noStyle>
            <InputNumber max={2020} min={1990} type='number'   />
          </Form.Item>
        </Form.Item>

        <Form.Item name="modePlayer" label="Mode Player">
          <Checkbox.Group>
            <Row>
              <Col span={13}>
                <Checkbox value="singlePlayer" style={{ lineHeight: '32px' }}>
                  Single Player
                </Checkbox>
              </Col>
              <Col span={13}>
                <Checkbox value="multiplayer" style={{ lineHeight: '32px' }}>
                  MultiPlayer
                </Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>

        <Form.Item
          name='image_url'
          label="Image URL"
          rules={[
            {
              required: true,
              message: 'input game url poster',
            },
          ]}
        >
          <Input.TextArea />
        </Form.Item>

        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 11 }}>
          <Button type="primary" htmlType="submit">
            {
              edit
              ? 'Edit' 
              : 'Create'
            }
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default CreateGame