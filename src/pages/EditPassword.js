import React, {useContext} from 'react'
import Axios from 'axios'
import { Form, Input, Button, message } from 'antd';
import {Context} from '../store'
import {useHistory} from 'react-router-dom'

const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 5 },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not validate email!',
    number: '${label} is not a validate number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

const EditPassword = () => {
  const [form] = Form.useForm()
  const [user,,,,,,action] = useContext(Context)
  const history = useHistory()

  const onFinish = val => {
    Axios.post(`https://backendexample.sanbersy.com/api/change-password`, {
      current_password: val.currentPassword, new_password: val.newPassword, new_confirm_password: val.confirm
    },
    {
      headers: {
        Authorization: `Bearer ${user.token}`
      }
    })
    .then(() => {
      form.resetFields()
      message.success('Password Success Changed')
    }).catch(({response}) => {
      form.resetFields()
      message.error(JSON.stringify(response.data));
      if(response.data.status === 'Token is Expired'){
        action.userLogout()
        history.push('/login')
      }
    });
  }
  return (
    <div style={{marginTop: '20px'}}>
      <h1 style={{textAlign: 'center', marginBottom: '30px'}}>Edit Password</h1>
      <Form {...layout} form={form} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
        <Form.Item
          name="currentPassword"
          label="Current Password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            }]}
          hasFeedback
          >
            <Input.Password />
          </Form.Item>

        <Form.Item
          name="newPassword"
          label="New Password"
          rules={[
            {
              required: true,
              message: 'Please input new password!',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || value.length >= 6) {
                  return Promise.resolve();
                }
                return Promise.reject('Password min 6 characters!');
              },
            }),
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="confirm"
          label="Confirm New Password"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please confirm your new password!',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('newPassword') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(`Password didn't match!`);
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 11 }}>
          <Button type="primary" htmlType="submit">
            Edit Password
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default EditPassword
