import React, {useEffect, useState} from 'react'
import Axios from 'axios'
import {useParams} from 'react-router-dom'
import {Row, Col, Image, Tag} from 'antd'

const DetailGame = () => {
  let {id} = useParams()
  const [game, setGame] = useState({})
  
  useEffect(() => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
    .then(({data}) => {
      console.log(data);
      setGame(data)
    }).catch((err) => {
      console.log(err.response);
    });
  }, [id])
  return (
    <Row style={{backgroundColor: 'aqua'}}>
      <Col span={7}>
        <Image src={game.image_url}/>
      </Col>
      <Col span={17} style={{paddingLeft: '30px'}}>
        <h1 style={{marginBottom: '0px'}}>{game.name} ({game.release})</h1>
        <p>{game.genre}</p>
        <h3>Platform:</h3>
        <p>{game.platform}</p>
        <h3>Mode:</h3>
        {
          game.singlePlayer === 1 && <Tag color="blue">Single Player</Tag>
        }
        {
          game.multiplayer === 1 && <Tag color="blue">MultiPlayer</Tag>
        }
      </Col>
    </Row>
  )
}

export default DetailGame
