import React, {useContext} from 'react'
import { Form, Input, Button, message } from 'antd';
import Axios from 'axios'
import {Context} from '../store'
import {Link, useHistory} from 'react-router-dom'

const layout = {
  labelCol: {
    span: 10,
  },
  wrapperCol: {
    span: 5,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 11,
    span: 6,
  },
};

const Login = () => {
  const [, setUser] = useContext(Context)
  const history = useHistory()
  const [form] = Form.useForm();

  const onFinish = ({email, password}) => {
    Axios.post(`https://backendexample.sanbersy.com/api/user-login`,
    { email, password }, { headers: {'Access-Control-Allow-Origin': true}})
    .then(({data}) => {
      message.success('Login Success :)')
      form.resetFields()
      let logUser = {...data.user, token: data.token}
      setUser(logUser)
      localStorage.setItem('user', JSON.stringify(logUser))
      history.push('/')
    }).catch(({response}) => {
      message.error(JSON.stringify(response.data));
    });
  };

  return (
    <div style={{marginTop: '20px'}}>
      <h1 style={{textAlign: 'center', marginBottom: '20px'}}>Login</h1>

      <Form
        {...layout}
        form={form}
        name="basic"
        onFinish={onFinish}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              type: 'email',
              message: 'Please input your Email!'
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Login
          </Button>
        </Form.Item>
        <p style={{textAlign: 'center'}}>Don't have an account? <Link to='/register'>Create free account</Link></p>
      </Form>
    </div>
  );
}

export default Login
