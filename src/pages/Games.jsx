import React, {useContext, useEffect} from 'react'
import { Row } from 'antd';
import GamesCard from '../components/GamesCard';
import {Context} from '../store'

const Games = () => {
  const [,,games,,,,action] = useContext(Context)
  
  useEffect(() => {
    action.fetchGames()
  }, [])

  return (
    <div>
      <h1>Games</h1>
      <Row>
        {
          games &&
          games.map((val, i) => {
            return (<GamesCard key={i} data={val} />)
          })
        }
      </Row>
    </div>
  )
}

export default Games
