import React from 'react';
import './App.css';
import 'antd/dist/antd.css'
import Store from './store'
import Routes from './routes'

function App() {
  return (
    <Store>
      <Routes />
    </Store>
  );
}

export default App;
