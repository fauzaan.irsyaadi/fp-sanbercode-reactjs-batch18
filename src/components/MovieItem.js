import React from 'react'

const MovieItem = ({data}) => {
  return (
    <div>
      <p>{data.id}</p>
      <p>{data.title}</p>
      <p>{data.genre}</p>
      <p>{data.image_url}</p>
      <p>{data.description}</p>
      <p>{data.year}</p>
      <p>{data.duration}</p>
      <p>{data.rating}</p>
      <p>{data.review}</p>
      <p>{data.created_at}</p>
      <p>{data.updated_at}</p>
      <br />
      <a href='/'>Edit</a><br />
      <a href='/'>Delete</a>
      <hr />
      <br />
    </div>
  )
}

export default MovieItem
