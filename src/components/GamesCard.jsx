import React from 'react'
import { Card } from 'antd';
import {Link} from 'react-router-dom'
const { Meta } = Card;

const GamesCard = ({data}) => {
  return (
    <Link to={`/games/${data.id}`} style={{margin: '40px', marginTop: '0'}}>
      <Card
        style={{ width: 300 }}
        cover={
          <img
            alt={`${data.name}`}
            src={data.image_url}
            width={250}
            height={400}
            style={{objectFit: 'cover'}}
          />
        }
      >
        <Meta
          title={data.name}
          description={data.release}
        />
        <Meta style={{marginTop: '10px'}}
          description={data.platform}
        />
      </Card>
    </Link>
  )
}

export default GamesCard
