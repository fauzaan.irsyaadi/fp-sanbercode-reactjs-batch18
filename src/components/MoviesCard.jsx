import React from 'react'
import { Card } from 'antd';
import {Link} from 'react-router-dom'

const { Meta } = Card;

const MoviesCard = ({data}) => {
  return (
    <Link to={`/movies/${data.id}`} div style={{margin: '40px', marginTop: '0'}}>
      <Card
        style={{ width: 300 }}
        cover={
          <img
            alt={`image ${data.name}`}
            src={data.image_url}
            width={250}
            height={400}
            style={{objectFit: 'cover'}}
          />
        }
      >
        <Meta
          title={data.title}
          description={data.year}
        />
        <Meta style={{marginTop: '10px'}}
          description={data.genre}
        />
      </Card>
    </Link>
  )
}

export default MoviesCard
