import React, {useContext} from 'react'
import { Form, Select, InputNumber, Button, Row, Col, Slider, Input } from 'antd';
import {Context} from '../store'
import Axios from 'axios'

const { Option } = Select;

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 14 },
};
const formSearchLayout = {
  wrapperCol: { span: 20 },
};

const FormFilterMovie = () => {
  const [,,,,,setMovies, action] = useContext(Context)
  const [form] = Form.useForm();

  const reset = () => {
    form.resetFields()
    action.fetchMovies()
  }

  const onFinish = value => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
    .then(({data}) => {
      let result = [...data]
      if(value.genre){
        result = result.filter(data => (String(data.genre)).toLowerCase().indexOf(value.genre.toLowerCase()) !== -1)
      }
      if(value.rating){
        result = result.filter(data => Number(data.rating) === Number(value.rating))
      }
      if(value.duration){
        result = result.filter(data => Number(data.duration) === Number(value.duration))
      }
      if(value.year){
        result = result.filter(data => Number(data.year) === Number(value.year))
      }
      setMovies(result);
    }).catch((err) => {
      console.log(err);
    })
  }
  const searchMovie = ({keySearch}) => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
    .then(({data}) => {
      if(keySearch){
        let result = data.filter((val) => (String(val.title).toLowerCase()).indexOf(keySearch.toLowerCase()) !== -1)
        setMovies(result)
      } else {
        setMovies(data)
      }
    }).catch((err) => {
      console.log(err);
    })
  }

  return (
    <div>
      <br />
      <h3>Search by Title:</h3>
      <Form
        name="search-form"
        {...formSearchLayout}
        onFinish={searchMovie}
      >
        <Form.Item name="keySearch">
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 14, offset: 7 }}>
          <Button type="primary" htmlType="submit">
            Search
          </Button>
        </Form.Item>
      </Form>
      <br />
      <h2>Filter:</h2>

      <Form
        form={form}
        name="nest-messages"
        {...formItemLayout}
        onFinish={onFinish}
      >
        <Form.Item name="genre" label="Genre">
          <Select placeholder="Select Movie genre">
            <Option value="action">Action</Option>
            <Option value="adventure">Adventure</Option>
            <Option value="animation">Animation</Option>
            <Option value="comedy">Comedy</Option>
            <Option value="crime">Crime</Option>
            <Option value="documentary">Documentary</Option>
            <Option value="drama">Drama</Option>
            <Option value="family">Family</Option>
            <Option value="fantasy">Fantasy</Option>
            <Option value="history">History</Option>
            <Option value="horor">Horor</Option>
            <Option value="music">Music</Option>
            <Option value="mistery">Mistery</Option>
            <Option value="romance">Romance</Option>
            <Option value="war">War</Option>
          </Select>
        </Form.Item>

        <Form.Item name="rating" label="Rating">
          <Slider
            max={10}
            marks={{
              0: '0',
              1: '1',
              2: '2',
              3: '3',
              4: '4',
              5: '5',
              6: '6',
              7: '7',
              8: '8',
              9: '9',
              10: '10',
            }}
          />
        </Form.Item>

        <Form.Item label="Duration" rules={[{type: 'number'}]}>
          <Form.Item name="duration" noStyle>
            <InputNumber max={1000} min={1} type='number' />
          </Form.Item>
        </Form.Item>

        <Form.Item label="Release" rules={[{type: 'number'}]}>
          <Form.Item name="year" noStyle>
            <InputNumber max={2020} min={1990} type='number' />
          </Form.Item>
        </Form.Item>

        <Form.Item>
          <Row justify='space-around'>
            <Col>
              <Button type="primary" htmlType="submit" >
                Submit
              </Button>
            </Col>
            <Col>
              <Button type="primary" onClick={reset} >
                Reset
              </Button>
            </Col>

          </Row>
        </Form.Item>
      </Form>
    </div>
  );
}

export default FormFilterMovie
