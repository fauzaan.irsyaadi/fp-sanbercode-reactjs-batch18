import React, {createContext, useState} from 'react'
import Axios from 'axios'

export const Context = createContext()

const Index = ({children}) => {
  let cekUser = JSON.parse(localStorage.getItem('user')) || {}
  const [user, setUser] = useState(cekUser)
  const [games, setGames] = useState(null)
  const [movies, setMovies] = useState(null)
  
  const userLogout = () => {
    setUser({})
    localStorage.removeItem('user')
  }

  const fetchGames = () => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-game`)
    .then(({data}) => {
      console.log('fetch Game');
      setGames(data)
    }).catch((err) => {
      console.log(err);
    });
  }

  const fetchMovies = () => {
    Axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
    .then(({data}) => {
      console.log('fetch movie');
      setMovies(data)
    }).catch((err) => {
      console.log(err);
    });
  }

  const action = {
    fetchGames, userLogout, fetchMovies
  }

  return (
    <Context.Provider value={[user, setUser, games, setGames, movies, setMovies, action]}>
      {children}
    </Context.Provider>
  )
}

export default Index
