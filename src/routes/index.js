import React, {useContext} from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import { Layout } from 'antd';

import Header from '../components/Header'
import Footer from '../components/Footer'
import {Context} from '../store'
import Home from '../pages/Home'
import Games from '../pages/Games'
import ManageGames from '../pages/ManageGames'
import Movies from '../pages/Movies'
import ManageMovie from '../pages/ManageMovie.jsx'
import Login from '../pages/Login'
import Register from '../pages/Register'
import EditPassword from '../pages/EditPassword'
import CreateGame from '../pages/CreateGame'
import CreateMovie from '../pages/CreateMovie';
import DetailGame from '../pages/DetailGame';
import DetailMovie from '../pages/DetailMovie';

const { Content } = Layout;

const Index = () => {
  const [user] = useContext(Context)

  const PrivateRoute = ({user, ...props}) => user.name ? <Route {...props} /> : <Redirect to='/login' />

  const LoginRoute = ({user, ...props}) => {
    if(user.name){
      return <Redirect to='/' />
    } else {
      return <Route {...props} />  
    }
  }

  return (
    <Router>
      <Layout>
        <Header />
        <Content>
          <div className="site-layout-content">
            <Switch >
                <Route exact path='/' component={Home} />
                <Route exact path='/games/manage' component={ManageGames} />
                <PrivateRoute exact path='/games/create' user={user} component={CreateGame} />
                <PrivateRoute exact path='/games/edit/:id' user={user} component={CreateGame} />
                <Route exact path='/movies/manage' component={ManageMovie} />
                <PrivateRoute exact path='/movies/create' user={user} component={CreateMovie} />
                <PrivateRoute exact path='/movies/edit/:id' user={user} component={CreateMovie} />
                <PrivateRoute exact path='/user/editpassword' user={user} component={EditPassword} />
                <LoginRoute exact path='/login' user={user} component={Login} />
                <LoginRoute exact path='/register' user={user} component={Register} />
                <Route exact path='/games' component={Games} />
                <Route exact path='/movies' component={Movies} />
                <Route exact path='/games/:id' component={DetailGame} />
                <Route exact path='/movies/:id' component={DetailMovie} />
            </Switch>
          </div>
        </Content>
        <Footer />
      </Layout>
    </Router>
  )
}

export default Index
